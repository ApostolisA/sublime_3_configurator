TEXT_BEGIN="// Settings in here override those in \"Default/Preferences.sublime-settings\",
// and are overridden in turn by file type specific settings.\n{"
FONT_LENGTH="\"font_size\": 8,\n"
WORD_WRAP="\"word_wrap\": true,\n"
TEXT_END="}"

TO_WRITE=$TEXT_BEGIN$FONT_LENGTH$WORD_WRAP$TEXT_END
printf "$TO_WRITE" > ~/.config/sublime-text-3/Packages/User/Preferences.sublime-settings

TEXT_BEGIN="[\n"
COMMENT="{ \"keys\": [\"ctrl+m\"], \"command\": \"toggle_comment\", \"args\": { \"block\": false } },\n"
OPEN_FILE="{ \"keys\": [\"ctrl+t\"], \"command\": \"show_overlay\", \"args\": {\"overlay\": \"goto\", \"show_files\": true} },\n"
FOCUS_GROUP="{ \"keys\": [\"alt+1\"], \"command\": \"focus_group\", \"args\": { \"group\": 0 } },\n{\"keys\": [\"alt+2\"], \"command\": \"focus_group\", \"args\": { \"group\": 1 } },\n{ \"keys\": [\"alt+3\"], \"command\": \"focus_group\", \"args\": { \"group\": 2 } },\n"
MOVE_TO_GROUP="{ \"keys\": [\"alt+shift+1\"], \"command\": \"move_to_group\", \"args\": { \"group\": 0 } },\n{\"keys\": [\"alt+shift+2\"], \"command\": \"move_to_group\", \"args\": { \"group\": 1 } },\n{ \"keys\": [\"alt+shift+3\"], \"command\": \"move_to_group\", \"args\": { \"group\": 2 } },\n"
LAYOUT_1="""{\"keys\": [\"ctrl+shift+1\"],\"command\": \"set_layout\",\"args\":{\"cols\": [0.0, 1.0],\"rows\": [0.0, 1.0],\"cells\": [[0, 0, 1, 1]]}},\n"""
LAYOUT_2="""{\"keys\": [\"ctrl+shift+2\"],\"command\": \"set_layout\",\"args\":{\"cols\": [0.0, 0.5, 1.0],\"rows\": [0.0, 1.0],\"cells\": [[0, 0, 1, 1], [1,0,2,1] ]}},\n"""
LAYOUT_3="""{\"keys\": [\"ctrl+shift+3\"],\"command\": \"set_layout\",\"args\":{\"cols\": [0.0, 0.33, 0.66, 1.0],\"rows\": [0.0, 1.0],\"cells\": [[0, 0, 1, 1], [1,0,2,1], [2,0,3,1] ]}},\n"""

TEXT_END="]"

TO_WRITE=$TEXT_BEGIN$COMMENT$OPEN_FILE$FOCUS_GROUP$MOVE_TO_GROUP$LAYOUT_1$LAYOUT_2$LAYOUT_3$TEXT_END
printf "$TO_WRITE" > ~/.config/sublime-text-3/Packages/User/Default\ \(Linux\).sublime-keymap